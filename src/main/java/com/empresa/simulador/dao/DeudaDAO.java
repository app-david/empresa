package com.empresa.simulador.dao;

import com.empresa.simulador.dto.Deuda;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeudaDAO extends MongoRepository<Deuda, String> {
}
