package com.empresa.simulador.dto;

public class TransaccionPago {

    private String numeroReferenciaDeuda;
    private String numeroDocumento;
    private String importeDeudaPagada;
    private String formaPago;

    public String getNumeroReferenciaDeuda() {
        return numeroReferenciaDeuda;
    }

    public void setNumeroReferenciaDeuda(String numeroReferenciaDeuda) {
        this.numeroReferenciaDeuda = numeroReferenciaDeuda;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getImporteDeudaPagada() {
        return importeDeudaPagada;
    }

    public void setImporteDeudaPagada(String importeDeudaPagada) {
        this.importeDeudaPagada = importeDeudaPagada;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }
}
