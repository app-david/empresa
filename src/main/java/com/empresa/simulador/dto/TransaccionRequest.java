package com.empresa.simulador.dto;

import com.empresa.simulador.dto.transaccion.ConsultaDeuda;

public class TransaccionRequest {

    private ConsultaDeuda consultaDeuda;

    public ConsultaDeuda getConsultaDeuda() {
        return consultaDeuda;
    }

    public void setConsultaDeuda(ConsultaDeuda consultaDeuda) {
        this.consultaDeuda = consultaDeuda;
    }
}
