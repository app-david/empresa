package com.empresa.simulador.dto.notificacion;

import com.empresa.simulador.dto.transaccion.RecaudosRq;

public class NotificacionPago {

    private RecaudosRq recaudosRq;

    public RecaudosRq getRecaudosRq() {
        return recaudosRq;
    }

    public void setRecaudosRq(RecaudosRq recaudosRq) {
        this.recaudosRq = recaudosRq;
    }
}
