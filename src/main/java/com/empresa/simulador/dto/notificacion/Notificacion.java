package com.empresa.simulador.dto.notificacion;

import com.empresa.simulador.dto.TransaccionPago;
import com.empresa.simulador.dto.transaccion.Transaccion;

public class Notificacion {

    private String codigoConvenio;
    private Transaccion transaccionPago;

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public Transaccion getTransaccionPago() {
        return transaccionPago;
    }

    public void setTransaccionPago(Transaccion transaccionPago) {
        this.transaccionPago = transaccionPago;
    }
}
