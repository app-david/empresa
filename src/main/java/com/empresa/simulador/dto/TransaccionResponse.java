package com.empresa.simulador.dto;

import com.empresa.simulador.dto.transaccion.ConsultaDeudaResponse;

public class TransaccionResponse {

    private ConsultaDeudaResponse consultaDeudaResponse;

    public ConsultaDeudaResponse getConsultaDeudaResponse() {
        return consultaDeudaResponse;
    }

    public void setConsultaDeudaResponse(ConsultaDeudaResponse consultaDeudaResponse) {
        this.consultaDeudaResponse = consultaDeudaResponse;
    }
}
