package com.empresa.simulador.dto.transaccion;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Transaccion {

    private String numeroReferenciaDeuda;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nombreCliente;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ListaDocumentos listaDocumentos;

    //pago
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String numeroDocumento;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String importeDeudaPagada;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String formaPago;

    public String getNumeroReferenciaDeuda() {
        return numeroReferenciaDeuda;
    }

    public void setNumeroReferenciaDeuda(String numeroReferenciaDeuda) {
        this.numeroReferenciaDeuda = numeroReferenciaDeuda;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public ListaDocumentos getListaDocumentos() {
        return listaDocumentos;
    }

    public void setListaDocumentos(ListaDocumentos listaDocumentos) {
        this.listaDocumentos = listaDocumentos;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getImporteDeudaPagada() {
        return importeDeudaPagada;
    }

    public void setImporteDeudaPagada(String importeDeudaPagada) {
        this.importeDeudaPagada = importeDeudaPagada;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }
}
