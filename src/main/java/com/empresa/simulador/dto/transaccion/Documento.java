package com.empresa.simulador.dto.transaccion;

public class Documento {

    private String numero;
    private String descripcion;
    private String fechaEmision;
    private String fechaVencimiento;
    private Double importaDeuda;
    private Double importaDeudaMinima;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Double getImportaDeuda() {
        return importaDeuda;
    }

    public void setImportaDeuda(Double importaDeuda) {
        this.importaDeuda = importaDeuda;
    }

    public Double getImportaDeudaMinima() {
        return importaDeudaMinima;
    }

    public void setImportaDeudaMinima(Double importaDeudaMinima) {
        this.importaDeudaMinima = importaDeudaMinima;
    }

}
