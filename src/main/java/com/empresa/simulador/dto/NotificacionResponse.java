package com.empresa.simulador.dto;

import com.empresa.simulador.dto.notificacion.NotificacionPago;
import com.empresa.simulador.dto.transaccion.ConsultaDeudaResponse;

public class NotificacionResponse {

    private ConsultaDeudaResponse notificarPagoResponse;

    public ConsultaDeudaResponse getNotificarPagoResponse() {
        return notificarPagoResponse;
    }

    public void setNotificarPagoResponse(ConsultaDeudaResponse notificarPagoResponse) {
        this.notificarPagoResponse = notificarPagoResponse;
    }
}
