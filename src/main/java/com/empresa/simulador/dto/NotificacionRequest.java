package com.empresa.simulador.dto;

import com.empresa.simulador.dto.notificacion.NotificacionPago;

public class NotificacionRequest {

    private NotificacionPago notificarPago;

    public NotificacionPago getNotificarPago() {
        return notificarPago;
    }

    public void setNotificarPago(NotificacionPago notificarPago) {
        this.notificarPago = notificarPago;
    }
}
