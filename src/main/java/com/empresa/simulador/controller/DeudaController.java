package com.empresa.simulador.controller;

import com.empresa.simulador.dto.*;
import com.empresa.simulador.dto.transaccion.ConsultaDeudaResponse;
import com.empresa.simulador.dto.transaccion.Documento;
import com.empresa.simulador.dto.transaccion.ListaDocumentos;
import com.empresa.simulador.dto.transaccion.Respuesta;
import com.empresa.simulador.service.DeudaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/simulador/v1/deuda")
public class DeudaController {

    AtomicInteger generarDocumento = new AtomicInteger(1);

    @Autowired
    private DeudaService deudaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void registrarDeuda(@RequestBody Deuda deuda){
        deudaService.crearDeuda(deuda);
    }

    @PostMapping("/consultarDeuda")
    public TransaccionResponse consultaDeuda(@RequestBody TransaccionRequest request){
        return deudaService.consultarDeuda(request);
    }

    @PostMapping("/notificarPago")
    public NotificacionResponse notificarPago(@RequestBody NotificacionRequest request){
        return deudaService.notificarPago(request);
    }
}
