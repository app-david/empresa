package com.empresa.simulador.service;

import com.empresa.simulador.dto.*;

import java.util.List;

public interface DeudaService {

    public void crearDeuda(Deuda deuda);
    public void actualizarDeuda(Deuda deuda, String estado);
    public TransaccionResponse consultarDeuda(TransaccionRequest request);
    public NotificacionResponse notificarPago(NotificacionRequest request);
}
