package com.empresa.simulador.service.impl;

import com.empresa.simulador.dao.DeudaDAO;
import com.empresa.simulador.dto.*;
import com.empresa.simulador.dto.transaccion.*;
import com.empresa.simulador.service.DeudaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeudaServiceImpl implements DeudaService {

    @Autowired
    private DeudaDAO deudaDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    private final double IMPORTE_DEUDA_MINIMA = 0.10;
    private final String ESTADO_PAGADO = "P";
    private final String ESTADO_PENDIENTE = "A";


    @Override
    public void crearDeuda(Deuda deuda) {
        deudaDAO.save(deuda);
    }

    @Override
    public void actualizarDeuda(Deuda deuda, String estado) {
        deuda.setEstado(estado);
        deudaDAO.save(deuda);
    }

    @Override
    public TransaccionResponse consultarDeuda(TransaccionRequest request){
        String numeroReferencia = request.getConsultaDeuda().getRecaudosRq().getDetalle().getTransaccion().getNumeroReferenciaDeuda();
        Criteria criteria = Criteria.where("numeroReferencia").is(numeroReferencia);
        Query query = new Query();
        query.addCriteria(criteria);

        List<Deuda> listaDeudas = mongoTemplate.find(query, Deuda.class);
        return simularRespuesta(request, listaDeudas);
    }

    public NotificacionResponse notificarPago(NotificacionRequest request) {
        String numeroReferencia = request.getNotificarPago().getRecaudosRq().getDetalle().getTransaccion().getNumeroReferenciaDeuda();
        String numero = request.getNotificarPago().getRecaudosRq().getDetalle().getTransaccion().getNumeroDocumento();

        Criteria criteria = Criteria.where("numeroReferencia").is(numeroReferencia).andOperator(Criteria.where("numero").is(numero));
        Query query = new Query();
        query.addCriteria(criteria);

        List<Deuda> listaDeudas = mongoTemplate.find(query, Deuda.class);
        Deuda deudaPagar = listaDeudas.get(0);
        this.actualizarDeuda(deudaPagar, ESTADO_PAGADO);

        return simularRespuestaNotificacion(request, deudaPagar);
    }

    private TransaccionResponse simularRespuesta(TransaccionRequest request, List<Deuda> listaDeudas){
        TransaccionResponse response = new TransaccionResponse();

        ConsultaDeudaResponse consultaDeuda = new ConsultaDeudaResponse();
        consultaDeuda.setRecaudosRs(request.getConsultaDeuda().getRecaudosRq());

        Respuesta respuesta = new Respuesta();

        List<Documento> listaDeudasPendientes = new ArrayList<>();
        if(listaDeudas != null && !listaDeudas.isEmpty()){
            String nombreCliente = listaDeudas.get(0).getNombreCliente();
            consultaDeuda.getRecaudosRs().getDetalle().getTransaccion().setNombreCliente(nombreCliente);
            for(Deuda deuda : listaDeudas){
                if(deuda.getEstado().equals(ESTADO_PENDIENTE)){
                    Documento documento = new Documento();
                    documento.setNumero(deuda.getNumero());
                    documento.setDescripcion(deuda.getDescripcion());
                    documento.setFechaEmision(deuda.getFechaEmision());
                    documento.setFechaVencimiento(deuda.getFechaVencimiento());
                    documento.setImportaDeuda(deuda.getImportaDeuda());
                    documento.setImportaDeudaMinima(IMPORTE_DEUDA_MINIMA);
                    listaDeudasPendientes.add(documento);
                }
            }

            if(listaDeudasPendientes.isEmpty()){
                respuesta.setCodigo("3009");
                respuesta.setDescripcion("No tiene deudas pendientes");
            } else {
                respuesta.setCodigo("0001");
                respuesta.setDescripcion("Transacción realizada con éxito");
            }
        } else {
            respuesta.setCodigo("0101");
            respuesta.setDescripcion("Número de referencia no existe");
        }
        consultaDeuda.getRecaudosRs().getDetalle().setRespuesta(respuesta);

        ListaDocumentos listaDocumentos = new ListaDocumentos();
        listaDocumentos.setDocumento(listaDeudasPendientes);

        //consultaDeuda.getRecaudosRs().getDetalle().getTransaccion().setNombreCliente("David Loyola");
        consultaDeuda.getRecaudosRs().getDetalle().getTransaccion().setListaDocumentos(listaDocumentos);

        response.setConsultaDeudaResponse(consultaDeuda);
        return response;
    }

    private NotificacionResponse simularRespuestaNotificacion(NotificacionRequest request, Deuda deudaPagada){
        NotificacionResponse response = new NotificacionResponse();

        ConsultaDeudaResponse notificarPagoResponse = new ConsultaDeudaResponse();
        notificarPagoResponse.setRecaudosRs(request.getNotificarPago().getRecaudosRq());

        Respuesta respuesta = new Respuesta();
        respuesta.setCodigo("001");
        respuesta.setDescripcion("Transacción realizada con éxito");
        notificarPagoResponse.getRecaudosRs().getDetalle().setRespuesta(respuesta);

        String numeroReferencia = notificarPagoResponse.getRecaudosRs().getDetalle().getTransaccion().getNumeroReferenciaDeuda();
        Transaccion transaccion = new Transaccion();
        transaccion.setNumeroReferenciaDeuda(numeroReferencia);

        notificarPagoResponse.getRecaudosRs().getDetalle().setTransaccion(transaccion);

        response.setNotificarPagoResponse(notificarPagoResponse);
        return response;
    }

}
